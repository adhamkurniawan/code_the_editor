// button linking to dashboard page
function goToDashboard() {
  location.href = "dashboard.html";
}

// calling ruler
var myRuler = new ruler({
  container: document.querySelector('#ruler'),// reference to DOM element to apply rulers on
  rulerHeight: 15, // thickness of ruler
  fontFamily: 'arial',// font for points
  fontSize: '7px', 
  strokeStyle: 'black',
  lineWidth: 1,
  enableMouseTracking: true,
  enableToolTip: true
});

// calling literrally canvas
LC.init(
  document.getElementsByClassName('literally core')[0],
  {imageURLPrefix: '../images/canvas'}
);

// toggleMenu
function toggleTools() {
  var Tools = document.getElementById('tools');
  
  // error handling
  try {
    if (Tools.style.display === 'block') {
      Tools.style.display = 'none';
    } else {
      Tools.style.display = 'block';
    }
  } catch (error) {
    console.log('Error: ' + error);
  }
}

// togglePhotos
function togglePhotos() {
  var Photos = document.getElementById('photos');

  // error handling
  try {
    if (Photos.style.display === 'block') {
      Photos.style.display = 'none';
    } else {
      Photos.style.display = 'block';
    }
  } catch (error) {
    console.log('Error: ' + error);
  }
}

// toggleProducts
function toggleProducts() {
  var Products = document.getElementById('products');

  // error handling
  try {
    if (Products.style.display === 'block') {
      Products.style.display = 'none';
    } else {
      Products.style.display = 'block';
    }
  } catch (error) {
    console.log('Error: ' + error);
  }
}

// zoomIn
function zoomIn() {
  
}